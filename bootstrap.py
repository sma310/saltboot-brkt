#!/usr/bin/python

###
#
# Steve Anthony sma310@lehigh.edu
# 08/09/2015
#
# Bootstrap Salt and convert Bracket tags to Salt grains.
# Set minion_id to Bracket instance_id and set master fqdn.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

import json
import os
import subprocess
import logging
import logging.handlers
import re

metadata='/etc/brkt-metadata.json'
grains='/etc/salt/grains'
salt_minionsd='/etc/salt/minion.d'
salt_id=salt_minionsd+'/id.conf'
salt_master=salt_minionsd+'/master.conf'
salt_url='brine.cc.lehigh.edu'

logger = logging.getLogger('MyLogger')
logger.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
logger.addHandler(handler)

# read in bracket metadata
f=open(metadata,'r')
x=json.load(f)
f.close()
logger.debug('bootstrap.py: loaded brkt-metadata.json')
roles=list()

# write out tags as grains for salt
f=open(grains,'w')
for key,value in (x['workload']['tags']).iteritems():
    # instance tags override workload tags
    if not key in x['instance']['tags']:
        m=re.search('role[0-9]+',key)
        if m:
            # append to the list of roles
            roles.append(value)
        else: # not a role, write to file
            f.write('brkt-'+key+': '+ value+'\n')
for key,value in (x['instance']['tags']).iteritems():
    m=re.search('role[0-9]+',key)
    if m:
        # append to the list of roles
        roles.append(value)
    else: # not a role, write to file
        f.write('brkt-'+key+': '+ value+'\n')
# write out roles
if len(roles) > 0:
    f.write('brkt-roles:\n')
    for role in roles:
        f.write('  - '+role+'\n')
# write workload-id
f.write('brkt-workload: '+x['workload']['id']+'\n')
f.close()
logger.debug('bootstrap.py: wrote '+grains)

# set a minion ID for host (instance-id)
if not os.path.exists(salt_minionsd):
    os.makedirs(salt_minionsd)
f=open(salt_id,'w')
f.write('id: '+x['instance']['id']+'\n')
f.close()
logger.debug('bootstrap.py: set minion id: '+x['instance']['id'])

# set master id
f=open(salt_master,'w')
f.write('master: '+salt_url+'\n')
f.close()
logger.debug('bootstrap.py: set master to'+salt_url)

#restart salt
try:
    proc = subprocess.call(["/etc/init.d/salt-minion","restart"])
    logger.debug('bootstrap.py: restarted salt-minion='+str(proc))
except subprocess.CalledProcessError, e:
    logger.debug('bootstrap.py: Exception calling process: '+e.strerror+' errno='+str(e.errno))
